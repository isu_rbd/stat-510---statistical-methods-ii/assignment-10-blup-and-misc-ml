---
title: "STAT 510 | HW 10"
author: "Ricardo Batista"
date: "4/14/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse); library(readr); library(knitr)
library(kableExtra); library(gridExtra); library(stringr)
library(latex2exp)
```

# Question 1

[Insert from evernote]


# Question 2

```{r echo=FALSE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
data <- read_delim("https://dnett.github.io/S510/hw10GenotypeYield.txt", delim = "\t")
```

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; Note the model implied is $Y = X\beta + \varepsilon$ with

$$
\begin{aligned}
\underset{101 \times 1}{\beta} &= (\mu, g_1, g_2, ..., g_{100})'
\\[10pt]
\underset{304 \times 101}{X} &= 
\begin{bmatrix}
\underset{304 \times 1}{1} & 
\underset{100 \times 100}{I} \otimes
\begin{bmatrix}
\underset{n_1 \times 1}{1} \\
\vdots
\\
\underset{n_{100} \times 1}{1}
\end{bmatrix}
\end{bmatrix}
&& n_i \text{ is # of plots with genotype i}
\end{aligned}
$$


# Question 3


```{r echo=FALSE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}


```
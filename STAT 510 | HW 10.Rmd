---
title: "STAT 510 | HW 10"
author: "Ricardo Batista"
date: "4/14/2020"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse); library(readr); library(knitr)
library(kableExtra); library(gridExtra); library(stringr)
library(latex2exp); library(MASS); library(pracma);
library(Matrix); library(robustbase)
```

# Question 1

[Insert from evernote]

\pagebreak

# Question 2

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
data <- read_delim("https://dnett.github.io/S510/hw10GenotypeYield.txt", delim = "\t") %>%
  as.data.frame() %>% mutate(genotype = as.factor(genotype))
n <- nrow(data)
```

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; Note the model implied is the GMMNE $Y = X\beta + \varepsilon$ with

$$
\begin{aligned}
\underset{101 \times 1}{\beta} &= (\mu, g_1, g_2, ..., g_{100})'
\\[10pt]
\underset{304 \times 1}{\varepsilon}  
&= 
(\varepsilon_{1,1}, \varepsilon_{1,2}, \varepsilon_{1,3}, \varepsilon_{2,1}, ..., \varepsilon_{100,4})
\\[10pt]
\underset{304 \times 101}{X} &= 
\begin{bmatrix}
\underset{304 \times 1}{1} & 
\underset{304 \times 100}
{
\begin{bmatrix}
\underset{n_1 \times 1}{1} & 0 & \cdots & 0\\
0 &\underset{n_2 \times 1}{1} & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots\\
0 & 0 & \cdots & \underset{n_{100} \times 1}{1}
\end{bmatrix}}
\end{bmatrix}.
\end{aligned}
$$

As such, we know the BLUE for $\beta$ is $\hat\beta = (X'X)^{-}X'y$, and the BLUE of $\mu + g_{i}$ is

$$
C\hat\beta
=
\begin{bmatrix}
\underset{100 \times 1}{1} &
\underset{100 \times 100}{I}
\end{bmatrix} \hat\beta.
$$

\vspace{15pt}

```{r echo=FALSE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}

y <- data$yield

X <- cbind(1, model.matrix(yield ~ 0 + genotype, data)) %>%
  as.data.frame() %>% rename(mu = V1) %>% as.matrix()

B_hat <- ginv(t(X) %*% X) %*% t(X) %*% y

C <- cbind(1, diag(100)) 

head(BLUE <- C %*% B_hat)

```

(Note we could've used R's full-rank formulation instead.)


## (b)

&nbsp;&nbsp;&nbsp;&nbsp; Now we have the linear mixed effects model $Y = 1\mu + Zg + e$ with

$$
\begin{aligned}
1 &= \underset{304 \times 1}{1}
\\[10pt]
g &= (g_1, g_2, ..., g_{100})' 
\\[10pt]
e &= (e_{1,1}, e_{1,2}, e_{1,3}, e_{2,1}, ..., e_{100,4})'
\\[10pt]
\underset{304 \times 100}{Z} &=
\begin{bmatrix}
\underset{n_1 \times 1}{1} & 0 & \cdots & 0\\
0 &\underset{n_2 \times 1}{1} & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots\\
0 & 0 & \cdots & \underset{n_{100} \times 1}{1}
\end{bmatrix}.
\end{aligned}
$$

$\underline{\text{The REML Method}}$

&nbsp;&nbsp;&nbsp;&nbsp; *Step 1*:

&nbsp;&nbsp;&nbsp;&nbsp; We seek $304 - \operatorname{rank}(1) = 303$ linearly independent vectors $a_i$ such that $a_i'1 = 0$. We will use 303 linearly independent rows of $I - P_1$:

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
X <- rep(1, n)
P_X <- X %*% solve(t(X) %*% X) %*% t(X)
I_min_PX <- diag(n) - P_X

# Remove rows to make it full rank
Aprime <- t(fullRank(t(diag(n) - P_X)))
```

&nbsp;&nbsp;&nbsp;&nbsp; *Step 2*:

&nbsp;&nbsp;&nbsp;&nbsp; Now, we find the maximum likelihood estimate of $\gamma = (\sigma_p^2, \sigma_e^2)$ using $w = A'y$. First, we express our model in Aitken form, i.e., $y = \mu1 + \epsilon$, with

$$
\begin{aligned}
\epsilon &= Zu + e \sim N(0, \sigma_e^2V)
\\[10pt]
\underset{304 \times 304}{V} &= 
\begin{bmatrix}
\underset{n_1 \times n_1}{B_1} & 0 & \cdots & 0\\
0 &\underset{n_2 \times n_2}{B_2} & \cdots & 0\\
\vdots & \vdots & \ddots & \vdots\\
0 & 0 & \cdots & \underset{n_{100} \times n_{100}}{B_{100}}
\end{bmatrix}
\\[10pt]
\underset{n_i \times n_i}{B_i} &=
\begin{bmatrix}
1 + \sigma_g^2/\sigma_e^2 & \sigma_g^2/\sigma_e^2 & \cdots & \sigma_g^2/\sigma_e^2\\
\sigma_g^2/\sigma_e^2 & 1 + \sigma_g^2/\sigma_e^2 & \cdots & \sigma_g^2/\sigma_e^2\\
\vdots & \vdots & \ddots & \vdots\\
\sigma_g^2/\sigma_e^2 & \sigma_g^2/\sigma_e^2 & \cdots & 1 + \sigma_g^2/\sigma_e^2
\end{bmatrix}.
\end{aligned}
$$

Note $\mu$ and $1$ defined as before. Let $\Sigma \equiv \sigma_e^2V$. Recall

$$
\ell(\gamma | w)
= -\frac{1}{2} \log \left\vert A'\Sigma A \right\vert -\frac{1}{2}w'(A'\Sigma A)^{-1}w -\frac{304 - 1}{2}\log(2\pi)
$$

and now we'll use a numerical method to produce $\hat\gamma_{MLE}$:  

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE, cache = TRUE}

A <- t(Aprime)
N <- as.vector(table(data$genotype))
w <- t(A) %*% y

negloglik <- function(gamma){
  
  # Producing Sigma
  s2_g <- gamma[1]; s2_e <- gamma[2]
  V <- bdiag()
  for(n_i in N){
    B_i <- diag(n_i) + s2_g/s2_e
    V <- bdiag(V, B_i)
  }
  
  S <- s2_e * as.matrix(V)
  
  term_1 <- -(1/2) * as.numeric(determinant(t(A) %*% S %*% A)$modulus)
  term_2 <- -(1/2) * t(w) %*% solve(t(A) %*% S %*% A) %*% w
  term_3 <- -(n - 1)/2 * log(2*pi)
  
  return(-(term_1 + term_2 + term_3))
  
}

gamma <- nlminb(rep(var(y), 2), negloglik, lower=c(0, 0))$par

kable(data.frame(t(gamma)), 
      booktabs = TRUE, escape = F, digits = 3,
      col.names = c("$\\sigma_{g}^2$", "$\\sigma_{e}^2$"),
      caption = "REML estimates for variance components") %>%
  kable_styling(latex_options = "hold_position")
```


## (c)

&nbsp;&nbsp;&nbsp;&nbsp; Recall the general version of the (estimated) BLUP for some random vector $g$ is

$$
\begin{aligned}
\hat{g} = \hat{G}Z'\hat\Sigma^{-1}\left(y - X\hat\beta_{\hat\Sigma}\right)
\end{aligned}
$$

with $\Sigma = ZGZ' + R$ defined per usual. For this particular setting, we have

$$
\begin{aligned}
G &= \sigma_g^2 \underset{100 \times 100}{I}  
\quad\quad 
R = \sigma_e^2 \underset{304 \times 304}{I}
\\[15pt]
\hat\Sigma &= Z\hat{G}Z' + \hat{R} = \hat\sigma_g^2 ZZ' + \hat\sigma_e^2I
\\[15pt]
\hat\beta_{\hat\Sigma} &= \left(1' {\hat{\Sigma}}^{-1} 1\right)^{-1}1'\hat\Sigma^{-1}y
\end{aligned}
$$

so 

$$
\begin{aligned}
\hat{g} = \hat\sigma_g^2Z'\hat\Sigma^{-1}\left(y - 1\hat\beta_{\hat\Sigma}\right).
\end{aligned}
$$

and the BLUP of $\mu + g$ is $\hat{\mu} + \hat{g} = \hat\beta_{\hat\Sigma} + \hat{g}$.

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE, cache = TRUE}
Z <- cbind(model.matrix(yield ~ 0 + genotype, data))
s2_g <- gamma[1]; s2_e <- gamma[2]
S <- s2_g * Z %*% t(Z) + s2_e * diag(n)
beta <- solve(t(X) %*% solve(S) %*% X) %*% t(X) %*% solve(S) %*% y

g <- s2_g * t(Z) %*% solve(S) %*%(y - X %*% beta)

BLUP <- as.numeric(beta) + g
rownames(BLUP) <- sprintf("mu + g_%d", 1:length(BLUP))
head(BLUP)
```


## (d)

&nbsp;&nbsp;&nbsp;&nbsp; Recall that the BLUE is $\bar y_i$, the mean yield for an individual genotype. The BLUP is a convex combination of $\bar y_i$ and $\hat\beta_{\hat\Sigma}$, which is very close to the mean. The result is shrinkage of the BLUE values towards the mean. Therefore, if the $i$th BLUE value is below the mean `r round(mean(y), 3)`, the corresponding BLUP value will be greater. This is why all the points to the left of $x =$ `r round(mean(y), 3)` are above $y = x$. Similar reasoning applies to the points to the right. 

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=5, message=FALSE, warning=FALSE, cache = TRUE}

BLUEvBLUP <- data.frame(BLUE, BLUP)

ggplot(BLUEvBLUP, aes(BLUE, BLUP)) + 
  geom_point() + geom_abline(slope = 1, intercept = 0)

```


## (e)

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=5, message=FALSE, warning=FALSE, cache = TRUE}
names(BLUE) <- rownames(g)
head(sort(BLUE, decreasing = TRUE), 5)
```

## (f)


```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=5, message=FALSE, warning=FALSE, cache = TRUE}
names(BLUP) <- rownames(g)
head(sort(BLUP, decreasing = TRUE), 5)
```


## (g)

&nbsp;&nbsp;&nbsp;&nbsp; Note that the highest rated genotype according to BLUE is "genotype 48." Since we only have a single observation for this genotype, its BLUE value will experience greater shrinkage towards the mean when computing the BLUP than similar values with larger sample sizes.  


# Question 3

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; The log likelihood is

$$
l(\theta_1, \theta_2|y) = \sum_{i = 1}^2 \left[ \sum_{i = 1}^7 y_{ij} \log(\theta_i) - 7\theta_i - \sum_{i = 1}^7 \log(y_{ij}!) \right]
$$

and we know that $\hat{\theta}_i = \bar y_i$. Then $l(\bar y_1, \bar y_2|y) = -37.108$ and

$$
AIC_{full} = -2 l(\hat\theta_1, \hat\theta_2|y) + 2(2) = 78.216
$$

## (b)

$$
BIC_{full} = -2 l(\hat\theta_1, \hat\theta_2|y) + 2 \log(14) = 79.494
$$

## (c)

&nbsp;&nbsp;&nbsp;&nbsp; The log likelihood is

$$
l(\theta|y) = \sum_{i = 1}^2 \sum_{i = 1}^7 y_{ij} \log(\theta) - 14 \theta - \sum_{i = 1}^2 \sum_{i = 1}^7 \log(y_{ij}!)
$$

and we know that $\hat{\theta} = \bar y$. Then $l(\bar y|y) = -40.302$ and

$$
AIC_{reduced} = -2 l(\hat\theta|y) + 2(1) = 82.605
$$

## (d)

$$
BIC_{reduced} = -2 l(\hat\theta|y) + (1)\log(14) = 83.244
$$

## (e)

&nbsp;&nbsp;&nbsp;&nbsp; The full model since its AIC is smaller.  


## (f)

&nbsp;&nbsp;&nbsp;&nbsp; Again, the full model (since its BIC is also smaller). 

## (g)

$$
-2\log(\Lambda)
= 
- 2\left[ l(\hat\theta|y) - l(\hat\theta_1, \hat\theta_2|y) \right]
= 
- 2\left[ -40.302 - (-37.108) \right]
= 6.389
$$

## (h)

&nbsp;&nbsp;&nbsp;&nbsp; Note $k_f - k_r = 2 - 1 = 1$ so $-2\log(\Lambda) \sim \cal{X}^2_1$. Therefore the p-value is

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
pchisq(6.389, 1, lower.tail = FALSE)
```

## (i)

&nbsp;&nbsp;&nbsp;&nbsp; Let $\theta = (\theta_1, \theta_2)'$ and note

$$
\begin{pmatrix} \bar{y}_{1\cdot} \\ \bar{y}_{2\cdot}\end{pmatrix}
\overset{\cdot}{\sim}
N \left(\begin{bmatrix} \theta_1 \\ \theta_2\end{bmatrix}, \hat{I}^{-1}(\hat\theta)\right).
$$

Now consider 

$$
\hat{I}(\hat{\theta})
=
\left.\frac{-\partial^{2} l(\theta | y)}{\partial \theta \partial \theta^{\prime}}\right|_{\theta=\hat{\theta}}
=\left[\begin{array}{cc}7/\hat{\theta}_{1} & 0 \\ 0 & 7/\hat{\theta}_{2}\end{array}\right]=\left[\begin{array}{cc} 7/\bar{y}_{1\cdot}  & 0 \\ 0 & 7/\bar{y}_{2\cdot}\end{array}\right]
$$

so $\bar{y}_{1\cdot} - \bar{y}_{2\cdot} \overset{\cdot}{\sim} N(\theta_1 - \theta_2, 7/\bar{y}_{1\cdot} + 7/\bar{y}_{2\cdot})$ and our test statistic is

$$
\frac{\bar{y}_{1\cdot} - \bar{y}_{2\cdot}}{\sqrt{\bar{y}_{1\cdot}/7 + \bar{y}_{2\cdot}/7}}
= -2.520.
$$

## (j)

&nbsp;&nbsp;&nbsp;&nbsp; Note the Wald statistic is distributed as a standard normal, so its p-value is

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
2*pnorm(-2.520)
```


\pagebreak

# Question 4

[Insert evernote]

\pagebreak


# Question 5

## (a)

&nbsp;&nbsp;&nbsp;&nbsp; Using Problem 4 and $\widehat{Var}(\hat\theta) = \widehat{I}^{-1}(\hat\theta)$, we have

$$
\begin{aligned}
\hat{\theta}_{MLE} &= \bar{y} = \frac{1}{n}\sum_{i = 1}^n = \frac{17}{100} = 0.17
\\[10pt]
\widehat{Var}(\hat\theta)
&= \frac{\bar{y}(1 - \bar{y})}{n} = \frac{(0.17)0.83}{100} = 0.001411.
\end{aligned}
$$

Therefore, by deck 22,

$$
\begin{aligned}
\hat{\theta}_{MLE} \pm z_{0.975} \sqrt{\widehat{Var}(\hat\theta)}
\quad \iff \quad
0.17 \pm 1.96 \sqrt{0.001411}
\quad \iff \quad
(0.0964, 0.2436)
\end{aligned}
$$

## (b)

&nbsp;&nbsp;&nbsp;&nbsp; Note that, by the previous problem, we have

$$
l(\theta | y)=\sum_{i=1}^{n} y_{i} \log \theta+\left(n-\sum_{i=1}^{n} y_{i}\right) \log (1-\theta)=17 \log (\theta)+83 \log (1-\theta).
$$

Now, to build the likelihood-based CI, we seek the values of $\theta$ with values of $l(\theta | y)$ above

$$
l(\hat\theta \vert y) - \frac{1}{2} \mathcal{X}_{1, 0.95}^2
=
-45.5886 - \frac{1}{2} 3.8415 = -47.5094.
$$

```{r echo=TRUE, fig.align="center", fig.height=3, fig.width=8, message=FALSE, warning=FALSE}
log_lik <- function(theta) 17*log(theta) + 83*log(1 - theta)

grid <- seq(0.001, 0.999, length.out = 10000)

values <- log_lik(grid)

interval_idx <- which(values >= -47.5094)

lower <- grid[min(interval_idx)]; upper <- grid[max(interval_idx)]

c(lower = lower, upper = upper)

```

\pagebreak

# Question 6

&nbsp;&nbsp;&nbsp;&nbsp; We use the same method as in the previous question. Namely, we have $y_{1,1}, ..., y_{1,350} \overset{iid}{\sim} \operatorname{Ber}(\theta_1)$ and $y_{2,1}, ..., y_{2,350} \overset{iid}{\sim} \operatorname{Ber}(\theta_2)$ so

$$
\begin{aligned}
\hat{\theta}_1 &= \bar{y}_{1 \cdot} = \frac{172}{350}
\\[10pt]
\widehat{Var}(\hat\theta_1) &= \frac{\bar{y}_{1 \cdot}(1 - \bar{y}_{1 \cdot})}{n} = 
\frac{\frac{172}{350}\left(1 - \frac{172}{350}\right)}{350}
\end{aligned}
$$

and

$$
\begin{aligned}
\hat{\theta}_1 &= \bar{y}_{2 \cdot} = \frac{137}{350}
\\[10pt]
\widehat{Var}(\hat\theta_2) &= \frac{\bar{y}_{2 \cdot}(1 - \bar{y}_{2 \cdot})}{n} = 
\frac{\frac{137}{350}\left(1 - \frac{137}{350}\right)}{350}.
\end{aligned}
$$

Therefore, one confidence interval for $\theta_1 - \theta_2$ is

$$
\begin{aligned}
&\widehat{\theta}_{1} -\widehat{\theta}_{2} \pm z_{0.975}
\sqrt{\widehat{\operatorname{Var}}\left(\widehat{\theta}_{1}-\widehat{\theta}_{2}\right)}
\\[10pt]
\iff &\widehat{\theta}_{1}-\widehat{\theta}_{2} \pm z_{0.975} \sqrt{\widehat{\operatorname{Var}}(\widehat{\theta}_{1})+\widehat{\operatorname{Var}}(\widehat{\theta}_{2})}
\\[10pt]
\iff &\frac{172}{350} - \frac{137}{350} \pm 1.96 
 \sqrt{\frac{\frac{172}{350}\left(1 - \frac{172}{350}\right)}{350} + \frac{\frac{137}{350}\left(1 - \frac{137}{350}\right)}{350}}
\\[10pt]
\iff & (0.027,0.173) 
\end{aligned}
$$

where the 2nd line follows from independence.  


